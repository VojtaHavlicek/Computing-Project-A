#define USE_MATH_DEFINES
#include "main.h"

using namespace std;
using namespace arma;

// CONSTANTS
// ---------------------------

// VARIABLES
// ----------------------------
   int steps;
double stepsize;
double damping;
double theta_0;

ofstream output("data");

// MAIN
// ----------------------------
int main(int argc, char** argv)
{
    init( &argc, argv);  
    // euler(steps, stepsize, theta_0, damping);
    // leapfrog(steps,0.4/*stepsize*/, theta_0, 0.0);
    // RK4(steps, 2.8, theta_0, 1.8);
    double_pendulum_RK4(1000, 0.01, 0.1, 0.0, 0.0, 0.0, 0.01, 1.0); // Change the parameters later

    //treshold_analysis_euler(0.01);  // Allow for 1% 
    output.close();
    return 0;
}

// PERFORMS AN EULER ITERATION
// ON THE SYSTEM.
// --------------------------
double euler(int steps_, double stepsize_, double theta_, double gamma_)
{
    // Update matrix
       mat updateMTX_ = euler_update_mtx(stepsize_, gamma_);
    
     // Check eigenvalues:
      // vec eigenvalues_ = eig_sym(updateMTX_);
      // cout << eigenvalues_ <<'\n';

    // Sequence of states to be stored:
    // State* sequence = new State[steps_]; 

    // Prepare the system
       vec initial_ =  zeros(2); //State(theta_, 0); 
           initial_(0) = theta_;

       Pendulum pendulum(initial_);

    // Iterate the method
       double analytic_omega_;
       double analytic_;
       double previous_error_ = 0;
       double current_error_;
       double amplification_factor_;

       for(int i = 0; i < steps_; ++i)
       {
           analytic_             = analytic(i, stepsize_, theta_, gamma_);
           analytic_omega_       = analytic_omega(i, stepsize_, theta_, gamma_);
           current_error_        = abs(pendulum.current(0) - analytic_);                                                                                                                    
           amplification_factor_ = previous_error_ == 0 ? 1 : (current_error_/previous_error_);

           output << (i*stepsize_) << '\t' << pendulum.current(0) << '\t' << analytic_ << '\t' << pendulum.current(1) << '\t' << analytic_omega_ << '\t' << current_error_ << '\t' << amplification_factor_ << '\n';

           previous_error_ = current_error_;
           pendulum.current = updateMTX_*pendulum.current; 
       }

       return current_error_;
}

// PERFORMS A LEAPFROG ITERATION
// -----------------------------
double leapfrog(int steps_, double stepsize_, double theta_, double gamma_)
{
    // Initial conditions
        vec initial_    = zeros(2);
            initial_(0) = theta_;

        Pendulum pendulum(initial_);

    // Initial Euler iteration
        output << 0 << '\t' << pendulum.current(0) << '\t' << analytic(0, stepsize_, theta_, gamma_) << '\t' << pendulum.current(1) << '\t' << 0 << '\t' << 0 <<'\n'; // output the initial state

        pendulum.current = euler_update_mtx(stepsize_, gamma_)*pendulum.initial;
        
    // Update matrix
        mat updateMTX_ = zeros(2,2);
        
        updateMTX_(0,1) =  2*stepsize_;
        updateMTX_(1,0) = -2*stepsize_;
        updateMTX_(1,1) = -2*stepsize_*gamma_;


    // Iterate
        double analytic_;
        double analytic_omega_;
        double current_error_;
        for(int i = 1; i < steps_; ++i)
        {
           analytic_       = analytic(i, stepsize_, theta_, gamma_);
           analytic_omega_ = analytic_omega(i, stepsize_, theta_, gamma_);
           current_error_  = abs(pendulum.current(0) - analytic_);                                                                                                            
           output << ((i)*stepsize_) << '\t' << pendulum.current(0) << '\t' << analytic_ << '\t' << pendulum.current(1) << '\t'<< analytic_omega_ << '\t' << abs(pendulum.current(0) - analytic_) << '\n'; 
           
           pendulum.next     = pendulum.previous + updateMTX_*pendulum.current;
           pendulum.previous = pendulum.current;
           pendulum.current  = pendulum.next;

        }

        return current_error_; 
}

// RUNGE-KUTTA 4 ITERATION
// ---------------------------------------------
double RK4(int steps_, double stepsize_, double theta_, double gamma_)
{
    vec initial_ = zeros(2);
        initial_(0) = theta_;

        Pendulum pendulum(initial_);
    
    // Prepare the update matrix 
    mat updateMTX_ = zeros(2,2);
        updateMTX_(0,1) =  1;
        updateMTX_(1,0) =  -1;
        updateMTX_(1,1) = -gamma_;

        //cout << updateMTX_ << '\n';
    
    // Prepare intermediate step vectors 
    vec k1 = zeros(2);
    vec k2 = zeros(2);
    vec k3 = zeros(2);
    vec k4 = zeros(2); 
    
    double analytic_;
    double analytic_omega_;
    double current_error_;
    for(int i = 0; i < steps; ++i)
    {
        k1 = stepsize_*updateMTX_*pendulum.current;
        k2 = stepsize_*updateMTX_*(pendulum.current + 0.5*k1);
        k3 = stepsize_*updateMTX_*(pendulum.current + 0.5*k2);
        k4 = stepsize_*updateMTX_*(pendulum.current + k3);
    
        analytic_omega_ = analytic_omega(i, stepsize_, theta_, gamma_);
        analytic_       = analytic(i, stepsize_, theta_, gamma_);
        current_error_  = abs(pendulum.current(0) - analytic_);                                                                                                            
        output << (i*stepsize_) << '\t' << pendulum.current(0) << '\t' << analytic_ << '\t' << pendulum.current(1) << '\t' << analytic_omega_ << '\t' << abs(pendulum.current(0)-analytic_) << '\n';  

        pendulum.current = pendulum.current + (1.0/6.0)*(k1 + 2*k2 + 2*k3 + k4);
    }
    return current_error_;
}

// PERFORMS SIMPLE STABILITY ANALYSIS BASED ON THE METHDO
// ------------------------------------------
void treshold_analysis(double treshold)
{
    double stepsize_lower_  = 0.01; // Lower limit 
    double stepsize_step_   = 0.01; // Stepsize
    double stepsize_upper_  = 2.01; // Upper limit
    
    double gamma_lower_    = 0.0;
    double gamma_stepsize_ = 0.02;
    double gamma_upper_    = 2.0;

    double time_ = M_PI*20.0;
    double stepsize_;
    double gamma_;

    for(int i = 0; i < 200; ++i)
    {
        stepsize_ =  stepsize_lower_ + stepsize_step_*i;
        
        for(int j = 0; j < 100; ++j)
        {
            gamma_ = gamma_lower_ + gamma_stepsize_*j;

            int N_ = int(time_/(stepsize_));
            
            cout << N_ << '\n';

            if(leapfrog(N_, stepsize_ , theta_0, gamma_) < treshold)
                output << stepsize_ << '\t' << gamma_ << '\n';
        }
    }
}

// RETURN AN ANALYTIC SOLUTION AT THE GIVEN STEP
// --------------------------------------------
double analytic(int step_, double stepsize_, double theta_, double gamma_ )
{
    double t_     = step_*stepsize_;
    double omega_ = sqrt(1-0.25*gamma_*gamma_); 
    double out_   = theta_*exp(-t_*gamma_/2)*(cos(omega_*t_) + sin(omega_*t_)*gamma_/(2*omega_));

    return out_;
}

// RETURN AN ANALYTIC SOLUTION TO OMEGA AT THE GIVEN STEP
// ------------------------------------------------------
double analytic_omega(int step_, double stepsize_, double theta_, double gamma_)
{
    double t_     = step_*stepsize_;
    double omega_ = sqrt(1-0.25*gamma_*gamma_);  // This is omega 0, constant
    double out_   = theta_*(exp(-gamma_*0.5*t_)*(gamma_*0.5*cos(omega_*t_) - omega_*sin(omega_*t_)) -gamma_*0.5*analytic(step_, stepsize_, theta_, gamma_));

    return out_;

}

// DOUBLE PENDULUM ITERATION PROCEDURE
// ------------------------------
double double_pendulum_RK4(int steps_, double stepsize_, double theta_, double phi_, double omega_, double nu_, double R_, double G_)
{   
    // Initial conditions vector
    vec initial_ = zeros(4);
        initial_(0) = theta_;
        initial_(1) = phi_;
        initial_(2) = omega_;
        initial_(3) = nu_;

    Pendulum pendulum(initial_);

    // Prepare the update matrix
    mat updateMTX_ = zeros(4,4);
        updateMTX_(0,2) = 1.0;
        updateMTX_(1,3) = 1.0;
        updateMTX_(2,0) = -(R_ + 1.0);
        updateMTX_(2,1) = R_;
        updateMTX_(2,2) = -G_;
        updateMTX_(3,0) = R_ + 1.0;
        updateMTX_(3,1) = -(R_ + 1.0);
        updateMTX_(3,2) = G_*(1.0-1.0/R_);
        updateMTX_(3,3) = -G_/R_;

    // Prepare the intermediate step vectors
        vec k1 = zeros(4);
        vec k2 = zeros(4);
        vec k3 = zeros(4);
        vec k4 = zeros(4);

        double initial_energy = double_pendulum_energy(pendulum.initial, R_);
        double total_energy   = initial_energy;

    // Iterate
        for(int i = 0; i < steps_; ++i)
        {
            k1 = stepsize_*updateMTX_*(pendulum.current);            
            k2 = stepsize_*updateMTX_*(pendulum.current + 0.5*k1); 
            k3 = stepsize_*updateMTX_*(pendulum.current + 0.5*k2); 
            k4 = stepsize_*updateMTX_*(pendulum.current + k3);     
         
            total_energy = double_pendulum_energy(pendulum.current, R_);

            // Output to the data file
            output << (i*stepsize_) << '\t' 
                   << pendulum.current(0) << '\t' 
                   << pendulum.current(1) << '\t' 
                   << pendulum.current(2) << '\t' 
                   << pendulum.current(3) << '\t' 
                   << double_pendulum_energy(pendulum.current, R_) << '\t'
                   << total_energy - initial_energy << '\n';
            
            
            pendulum.current = pendulum.current + (1.0/6.0)*(k1 + 2.0*k2 + 2.0*k3 + k4);                                                                   
        }

        return abs(total_energy-initial_energy);
}

// RETURNS THE TOTAL ENERGY OF DOUBLE PENDULUM SYSTEM
// --------------------------------------------------
double double_pendulum_energy(vec state, double R_)
{
    double theta_ = state(0);
    double phi_   = state(1);
    double omega_ = state(2);
    double nu_    = state(3);

    double out_ = 0.5*(omega_*omega_) +0.5*(R_*(omega_*omega_ + nu_*nu_ + 2.0*nu_*omega_*cos(phi_-theta_))) + 2.0 - cos(theta_) + R_*(2.0-cos(theta_) - cos(phi_));

    return out_; 
}


// RETURNS AN EULER UPDATE MATRIX
// -------------------------------
mat euler_update_mtx(double stepsize_, double damping_)
{
    mat updateMTX_  = zeros(2, 2); // Create 2x2 update matrix for the sys. 
                                                                            
    // Populate:                                                            
       updateMTX_(0,0) = 1.0;                                               
       updateMTX_(0,1) =  stepsize_;                                         
       updateMTX_(1,0) = -stepsize_;                                        
       updateMTX_(1,1) = 1.0-damping_*stepsize_;                             

    return updateMTX_;
}

// INITS & PARSES OPTIONAL ARGUMENTS
// FROM COMMANDLINE TO THE PROGRAM
// --------------------------
void init(int *argc, char** argv)
{
    // initialize problem variables:
    steps    = 1000;                                       
    stepsize = 0.1;                                   
    damping  = 0.0;
    theta_0  = 0.125*M_PI; // 1/8 PI
                                                      
    // parse the arguments simply as [steps stepsize gamma] 
    cout << *argc << '\n';                             
                                                      
    if(*argc > 1){                                     
        steps    = atoi(argv[1]);                     
        stepsize = atof(argv[2]);                     
        damping  = atof(argv[3]);
        theta_0  = atof(argv[4]);
    }                                                 

}



