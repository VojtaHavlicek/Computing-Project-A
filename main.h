// INCLUDES
//-----------------------------------------------------
#include<iostream>
#include<fstream>
#include<cstdlib>

#include<armadillo> // using Armadillo for matlab-like manipulations of matrices & vectors. The states are stored simply as 2x1 armadillo column vectors (class vec) in a [theta, omega] format.

using namespace arma; // Armadillo namespace

// MAIN PROCEDURES - DECLARATIONS
// ----------------------------------------------------
double       euler(int steps_, double stepsize_, double theta_, double gamma_); 
double    leapfrog(int steps_, double stepsize_, double theta_, double gamma_);
double         RK4(int steps_, double stepsize_, double theta_, double gamma_);

  void treshold_analysis(double treshold);

double analytic(int step_, double stepsize_, double theta_, double gamma_);
double analytic_omega(int step_, double stepsize_, double theta_, double gamma_);
   mat euler_update_mtx(double stepsize_, double damping_);

double double_pendulum_RK4(int steps_, double stepsize_, double theta_, double phi_, double omega_, double nu_, double R_, double G_);
double double_pendulum_energy(vec state, double R_);

void init(int* argc, char** argv);



// CLASS & STRUCT HEADERS:
//-----------------------------------------------------

// STATE
// The state of a single pendulum system
//
// TODO: integrate this with armadillo 2D vector
//       if possible
//
// struct State
// {
//    public:
//        double theta;
//        double omega;
//    
//        arma::vec vector(); // return a vector of [theta, omega]
//        
// 
//        State();
//        State(double theta_, double omega_);
// };


// PENDULUM CLASS
// Encapsulates the pendulum system. 

class Pendulum
{
    public:
        // system properties: 
        vec initial;        
        vec current;        
        vec previous;       
        vec next;
        
        // constructors:
        Pendulum();
        Pendulum(vec init);
    
    protected:

    private:

};


